/*
 * MasterLib_Rev.cpp
 *
 *  Created on: 04/04/2015
 *      Author: Thiago Mallon
 */

#include "MasterLib_Rev.h"

void ADC_Lib::ADC_init(void) {
	/* configurações gerais */
	LPC_SC->PCONP |= (1 << 12); // liga periférico ADC
	LPC_SC->PCLKSEL0 &= ~(0b11 << 24); // PCLK_ADC = C_CLK / 4
	LPC_PINCON->PINSEL3 |= (0b11 << 30); // pino como AD.5
	LPC_PINCON->PINMODE3 = ((LPC_PINCON->PINMODE3 & ~(0b11 << 30))
			| (0b10 << 30)); // desabilita pull-up e pull-down do pino
	/* configurações de operação */
	LPC_ADC->ADCR |= (1 << 5 | 1 << 8 | 1 << 16 | 1 << 21); // CLKDIV = 1 (PCLK_ADC / 2); modo burst; ADC em operação
	/* configurações de interrupção */
	LPC_ADC->ADINTEN |= (1 << 5); // habilita interrupção no pino
	NVIC->IP[22] |= (0 << 5 | 2 << 3); // nível 0 e subnível 2 de prioridade de interrupção
	NVIC->ISER[0] |= (1 << 22); // habilita interrupções no ADC
	NVIC->ICPR[0] |= (1 << 22); // retira qualquer pendência de interrupção do ADC
}

void PWM_Lib::PWM_init(void) {
	/* configurações gerais */
	LPC_SC->PCONP |= (1 << 6); // liga periférico PWM1
	LPC_SC->PCLKSEL0 = ((LPC_SC->PCLKSEL0 & ~(0b11 << 12)) | (0b01 << 12)); // PCLK_PWM1 = C_CLK / 4
	/* configurações de operação */
	LPC_PWM1->CTCR &= ~(0b11 << 0); // fonte de incremento é PCLK_PWM1
	LPC_PWM1->PR = 1; // atribui valor de prescaler
	LPC_PWM1->MR0 = 999999; // atribui valor de comparação do canal 0
	LPC_PWM1->MR1 = 23499; // atribui valor de comparação do canal 1
	LPC_PWM1->LER |= (1 << 0 | 1 << 1); // permite alteração dos canais de comparação, após coincidência de comparação no canal 0
	/* configurações de interrupção */
	LPC_PWM1->MCR |= (1 << 0 | 1 << 3); // habilita interrupção por coincidência nos canais 0 e 1
	NVIC->IP[9] |= (0 << 5 | 1 << 3); // nível 0 e subnível 1 de prioridade de interrupção
	NVIC->ISER[0] |= (1 << 9); // habilita interrupções no PWM1
	NVIC->ICPR[0] |= (1 << 9); // retira qualquer pendência de interrupção no PWM1
	/* inicializa o PWM1 */
	LPC_PWM1->TCR |= (1 << 3 | 1 << 0); // modo PWM; inicializa contadores
}

// adc, external-int, i2c, pull-up, pwm, qei, spi, systick, timer, uart, rit
void SPI_Lib::SPI_init(void) {
	/* configurações gerais */
	LPC_SC->PCONP |= (1 << 8); // liga periférico SPI
	LPC_SC->PCLKSEL0 &= ~(0b11 << 16); // PCLK_SPI = C_CLK / 4
	/* configurações de operação */
	LPC_SPI->SPCR |= (1 << 5); // modo master
	LPC_SPI->SPCR &= ~(1 << 6 | 1 << 4 | 1 << 3 | 1 << 2); //  transferência pelo MSB; CPOL = 0; CPHA = 0; dado de 8 bits
}

/* configura pinos do SPI */
void SPI_Lib::SPI_pinsConfig(void) {
	/* CLK */
	LPC_PINCON->PINSEL0 |= (0b11 << 30); // pino como CLK
	LPC_PINCON->PINMODE0 = ((LPC_PINCON->PINMODE0 & ~(0b11 << 30))
			| (0b10 << 30)); // desabilita pull-up e pull-down
	/* MOSI */
	LPC_PINCON->PINSEL1 |= (0b11 << 4); // pino como MOSI
	LPC_PINCON->PINMODE1 =
			((LPC_PINCON->PINMODE1 & ~(0b11 << 4)) | (0b11 << 4)); // desabilita pull-up e pull-down
	/* MISO */
	LPC_PINCON->PINSEL1 |= (0b11 << 2); // pino como MISO
	LPC_PINCON->PINMODE1 =
			((LPC_PINCON->PINMODE1 & ~(0b11 << 2)) | (0b11 << 2)); // desabilita pull-up e pull-down
}

/* envia dado via SPI */
void SPI_Lib::SPI_send(uint8_t data) {
	LPC_SPI->SPDR = data; // coloca dado no buffer TX
	do { // aguarda até que dado seja enviado
	} while (!(LPC_SPI->SPSR & (1 << 7)));
}

/* lê dado via SPI */
uint8_t SPI_Lib::SPI_read(void) {
	LPC_SPI->SPDR = 0x00; // dummy mode de envio
	do { // aguarda até que dado seja enviado
	} while (!(LPC_SPI->SPSR & (1 << 7)));
	return LPC_SPI->SPDR; // retorna valor recebido
}

/* configura SysTick */
void SysTick_Lib::SysTick_config(void) {
	SysTick->CTRL |= (1 << 2); // fonte de decremento é C_CLK
}

/* implementa delay em microssegundos */
void SysTick_Lib::delayUs(uint32_t us) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // atribui valor de reinício para contador
	static uint32_t count; // contador de interrupções (ou 'zeramentos')
	SysTick->CTRL |= (1 << 0); // inicializa contador
	for (count = 0; count < us; count++) { // executa até que valor de count se iguale ao valor de us
		do { // aguarda até que flag de interrupção esteja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador
}

/* implementa delay em milissegundos */
void SysTick_Lib::delayMs(uint32_t ms) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // atribui valor de reinício para contador
	static uint32_t count; // contador de interrupções (ou 'zeramentos')
	SysTick->CTRL |= (1 << 0); // inicializa contador
	for (count = 0; count < ms; count++) { // executa até que valor de count se iguale ao valor de ms
		do { // aguarda até que flag de interrupção esteja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador

}

/* configura/inicializa o TIMER0 */
void TIMER0_Lib::TIMER0_init(void) {
	/* configurações gerais */
	LPC_SC->PCONP |= (1 << 1); // liga periférico TIMER0
	LPC_SC->PCLKSEL0 = ((LPC_SC->PCLKSEL0 & ~(0b11 << 2)) | (0b01 << 2)); // PCLK_TIMER0 = C_CLK
	/* configurações de operação */
	LPC_TIM0->CTCR &= ~(0b11 << 0); // fonte de incremento é PCLK_TIMER0
	LPC_TIM0->PR = 1; // atribui valor do prescaler
	LPC_TIM0->MR0 = 999999; // atribui valor ao canal de comparação 0
	LPC_TIM0->MR1 = 23499; // atribui valor ao canal de comparação 1
	LPC_TIM0->MCR = ((LPC_TIM0->MCR & ~(0b11 << 1 | 0b11 << 4)) | (0b01 << 1)); // reinicia contadores após coincidência no canal 0
	/* configurações de interrupção */
	LPC_TIM0->MCR |= (1 << 0 | 1 << 3); // habilita interrupção por coincidência de comparação nos canais 0 e 1
	NVIC->IP[1] |= (0 << 5 | 0 << 3); // nível 0 e subnível 0 de prioridade de interrupção
	NVIC->ISER[0] |= (1 << 1); // habilita interrupções no TIMER0
	NVIC->ICPR[0] |= (1 << 1); // retira qualquer pendência de interrupção no TIMER0
}

/* configura/inicializa a UART3 */
void UART3_Lib::UART3_init(void) {
	/* configurações gerais */
	LPC_SC->PCONP |= (1 << 25); // liga periférico UART3
	LPC_SC->PCLKSEL1 &= ~(0b11 << 18); // PCLK_UART3 = C_CLK / 4
	LPC_PINCON->PINSEL9 |= (0b11 << 24 | 0b11 << 26);
	/* configurações do BAUD RATE */
	LPC_UART3->LCR |= (1 << 7); // seta DLAB - permite acesso aos divisores do BRG
	LPC_UART3->DLL = ((SystemCoreClock / 4) / (16 * 9600)); // LSB do divisor principal do BRG
	LPC_UART3->DLM = 0; // MSB do divisor principal do BRG
	LPC_UART3->FDR |= (1 << 4); // divisor fracional do BRG
	LPC_UART3->LCR &= ~(1 << 7); // limpa DLAB - bloqueia acesso aos divisores do BRG
	/* configurações do pacote */
	LPC_UART3->LCR |= (0b11 << 0); // dado trafegado será de 8 bits
	LPC_UART3->LCR &= ~(1 << 2 | 1 << 3); // 1 bit de parada; sem bit de paridade
	LPC_UART3->FCR |= (1 << 0 | 1 << 1 | 1 << 2); // ativa e reinicia FIFOs
	LPC_UART3->THR = 0; // limpa buffer TX
	LPC_UART3->RBR; // limpa buffer RX
	LPC_UART3->LCR &= ~(0b11 << 6); // nível de disparo de 1 dado
}

/* envia dado para buffer TX da UART3 */
void UART3_Lib::UART3_send(unsigned char data) {
	do { //	aguarda até que buffer TX possa receber novos dados
	} while (!(LPC_UART3->LSR & (1 << 5)));
	LPC_UART3->THR = data; // coloca dado no buffer TX
}

/* envia ponteiro para buffer TX da UART3 */
void UART3_Lib::UART3_sendPointer(const char* dataPtr) {
	while (*dataPtr != '\0') { // enquanto valor de posição de ponteiro não for igual fim de string, executa
		this->UART3_send(*dataPtr); // envia dado para buffer TX
		dataPtr++; // incrementa ponteiro
	}
}
