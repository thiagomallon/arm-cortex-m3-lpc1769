/*
 * MasterLib.cpp
 *
 *  Created on: 03/04/2015
 *      Author: Thiago Mallon
 */

#include "MasterLib.h"

/* configura SysTick */
void SysTickLib::SysTickConfig(void) {
	SysTick->CTRL |= (1 << 2); // fonte de decremento é C_CLK
}

/* implementa delay em microssegundos */
void SysTickLib::delayUs(uint32_t us) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // atribui valor de reinício para o contador
	static uint32_t count; // contador de interrupções
	SysTick->CTRL |= (1 << 0); // inicializa contador do SysTick
	for (count = 0; count < us; count++) { // executa até que valor do count se iguale ao valor de us
		do { // aguarda até que flag de interrupção seja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador do SysTick
}

/* implementa delay em milissegundos */
void SysTickLib::delayMs(uint32_t ms) {
	SysTick->LOAD = (SystemCoreClock / 1000) - 1; // atribui valor de reinício para o contador
	static uint32_t count; // contador de interrupções
	SysTick->CTRL |= (1 << 0); // inicializa contador do SysTick
	for (count = 0; count < ms; count++) { // executa até que valor do count se iguale ao valor de us
		do { // aguarda até que flag de interrupção seja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador do SysTick
}

/* configura/inicializa a UART3 */
void UART3Lib::UART3Init(void) {
	/* configurações gerais */
	LPC_SC->PCONP |= (1 << 25); // liga periférico UART3
	LPC_SC->PCLKSEL1 &= ~(0b11 << 18); // PCLK_UART3 = C_CLK / 4
	LPC_PINCON->PINSEL9 |= (0b11 << 24 | 0b11 << 26);
	/* configurações do BAUD RATE */
	LPC_UART3->LCR |= (1 << 7); // seta DLAB - permite acesso aos divisores do BRG
	LPC_UART3->DLL = (SystemCoreClock / (16 * 9600)); // LSB do divisor principal do BRG
	LPC_UART3->DLM = 0; // MSB do divisor principal do BRG
	LPC_UART3->FDR |= (1 << 4); // divisor fracional do BRG
	LPC_UART3->LCR &= ~(1 << 7); // limpa DLAB - bloqueia acesso aos divisores do BRG
	/* configurações do pacote */
	LPC_UART3->LCR |= (0b11 << 0); // dado trafegado será de 8 bits
	LPC_UART3->LCR &= ~(1 << 2 | 1 << 3); // 1 bit de parada; sem bit de paridade
	LPC_UART3->FCR |= (1 << 0 | 1 << 1 | 1 << 2); // ativa e reinicia FIFOs
	LPC_UART3->THR = 0; // limpa buffer TX
	LPC_UART3->RBR; // limpa buffer RX
	LPC_UART3->LCR &= ~(0b11 << 6); // nível de disparo de 1 dado
}

/* envia dado para buffer TX da UART3 */
void UART3Lib::UART3Send(char data) {
	do { //	aguarda até que buffer TX possa receber novos dados
	} while (!(LPC_UART3->LSR(1 << 5)));
	LPC_UART3->THR = data; // coloca dado no buffer TX
}

/* envia ponteiro para buffer TX da UART3 */
void UART3Lib::UART3SendPointer(const char* dataPtr) {
	while (*dataPtr != '\0') {
		this->UART3Send(*dataPtr);
		dataPtr++;
	}
}

