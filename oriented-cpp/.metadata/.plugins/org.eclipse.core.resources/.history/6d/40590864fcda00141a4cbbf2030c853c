/*
 * MasterLib_Rev.cpp
 *
 *  Created on: 04/04/2015
 *      Author: Thiago Mallon
 */

#include "MasterLib_Rev.h"

void ADC_Lib::ADC_init(void) {
	/* configurações gerais */
	LPC_SC->PCONP |= (1 << 12); // liga periférico ADC
	LPC_SC->PCLKSEL0 &= ~(0b11 << 24); // PCLK_ADC = C_CLK / 4
	LPC_PINCON->PINSEL3 |= (0b11 << 30); // pino como AD.5
	LPC_PINCON->PINMODE3 = ((LPC_PINCON->PINMODE3 & ~(0b11 << 30))
			| (0b10 << 30)); // desabilita pull-up e pull-down do pino
	/* configurações de operação */
	LPC_ADC->ADCR |= (1 << 5 | 1 << 8 | 1 << 16 | 1 << 21); // CLKDIV = 1 (PCLK_ADC / 2); modo burst; ADC em operação
	/* configurações de interrupção */
	LPC_ADC->ADINTEN |= (1 << 5); // habilita interrupção no pino
	NVIC->IP[22] |= (0 << 5 | 2 << 3); // nível 0 e subnível 2 de prioridade de interrupção
	NVIC->ISER[0] |= (1 << 22); // habilita interrupções no ADC
	NVIC->ICPR[0] |= (1 << 22); // retira qualquer pendência de interrupção do ADC
}

void PWM_Lib::PWM_init(void) {
	/* configurações gerais */
	LPC_SC->PCONP |= (1 << 6); // liga periférico PWM1
	LPC_SC->PCLKSEL0 = ((LPC_SC->PCLKSEL0 & ~(0b11 << 12)) | (0b01 << 12)); // PCLK_PWM1 = C_CLK / 4
	/* configurações de operação */
	LPC_PWM1->CTCR &= ~(0b11 << 0); // fonte de incremento é PCLK_PWM1
	LPC_PWM1->PR = 1; // atribui valor de prescaler
	LPC_PWM1->MR0 = 999999; // atribui valor de comparação do canal 0
	LPC_PWM1->MR1 = 23499; // atribui valor de comparação do canal 1
	LPC_PWM1->LER |= (1 << 0 | 1 << 1); // permite alteração dos canais de comparação, após coincidência de comparação no canal 0
	/* configurações de interrupção */
	LPC_PWM1->MCR |= (1 << 0 | 1 << 3); // habilita interrupção por coincidência nos canais 0 e 1
	NVIC->IP[9] |= (0 << 5 | 1 << 3); // nível 0 e subnível 1 de prioridade de interrupção
	NVIC->ISER[0] |= (1 << 9); // habilita interrupções no PWM1
	NVIC->ICPR[0] |= (1 << 9); // retira qualquer pendência de interrupção no PWM1
	/* inicializa o PWM1 */
	LPC_PWM1->TCR |= (1 << 3 | 1 << 0); // modo PWM; inicializa contadores
}
// adc, external-int, i2c, pull-up, pwm, qei, spi, systick, timer, uart, rit
void SysTick_Lib::SysTick_config(void) {
	SysTick->CTRL |= (1 << 2); // fonte de decremento é C_CLK
}

void SysTick_Lib::delayUs(uint32_t us) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // atribui valor de reinício para contador
	static uint32_t count; // contador de interrupções (ou 'zeramentos')
	SysTick->CTRL |= (1 << 0); // inicializa contador
	for (count = 0; count < us; count++) { // executa até que valor de count se iguale ao valor de us
		do { // aguarda até que flag de interrupção esteja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador
}

void SysTick_Lib::delayMs(uint32_t ms) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // atribui valor de reinício para contador
	static uint32_t count; // contador de interrupções (ou 'zeramentos')
	SysTick->CTRL |= (1 << 0); // inicializa contador
	for (count = 0; count < ms; count++) { // executa até que valor de count se iguale ao valor de ms
		do { // aguarda até que flag de interrupção esteja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador

}
