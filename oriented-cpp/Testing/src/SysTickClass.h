/*
 * SysTickClass.h
 *
 *  Created on: 09/04/2015
 *      Author: Thiago Mallon
 */

#ifndef SYSTICKCLASS_H_
#define SYSTICKCLASS_H_

#include "LPC17xx.h"

class SysTickClass {
public:
	void config(void);
	void delayUs(uint32_t);
};

#endif /* SYSTICKCLASS_H_ */
