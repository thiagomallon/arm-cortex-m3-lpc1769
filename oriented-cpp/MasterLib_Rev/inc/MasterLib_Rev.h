/*
 * MasterLib_Rev.h
 *
 *  Created on: 04/04/2015
 *      Author: Thiago Mallon
 */

#ifndef MASTERLIB_REV_H_
#define MASTERLIB_REV_H_

#include <LPC17xx.h>

class ADC_Lib {
private:
protected:
public:
	void ADC_init(void);
};

class I2C_Lib {
private:
protected:
public:
	void I2C_init(void);
	void I2C_send(void);
	void I2C_read(void);
};

class PWM_Lib {
private:
protected:
public:
	void PWM_init(void);
};

class QEI_Lib {
private:
protected:
public:
	void QEI_init(void);
};

class RIT_Lib {
private:
protected:
public:
	void RIT_config(void);
	void delayUs(uint32_t);
	void delayMs(uint32_t);
};

class SPI_Lib {
private:
protected:
public:
	void SPI_init(void);
	void SPI_pinsConfig(void);
	void SPI_send(uint8_t);
	uint8_t SPI_read(void);
};

class SysTick_Lib {
private:
protected:
public:
	void SysTick_config(void);
	void SysTick_delayUs(uint32_t);
	void SysTick_delayMs(uint32_t);
};

class TIMER0_Lib {
private:
protected:
public:
	void TIMER0_init(void);
};

class UART3_Lib {
private:
protected:
public:
	void UART3_init(void);
	void UART3_send(unsigned char);
	void UART3_sendPointer(const char*);
};

class MasterLib_Rev: public ADC_Lib,
		public I2C_Lib,
		public PWM_Lib,
		public QEI_Lib,
		public RIT_Lib,
		public SPI_Lib,
		public SysTick_Lib,
		public TIMER0_Lib,
		public UART3_Lib { // herda das demais classes declaradas no arquivo
private:
protected:
public:
};

#endif /* MASTERLIB_REV_H_ */
