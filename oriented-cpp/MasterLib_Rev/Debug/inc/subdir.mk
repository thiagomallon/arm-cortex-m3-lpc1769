################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../inc/MasterLib_Rev.cpp 

OBJS += \
./inc/MasterLib_Rev.o 

CPP_DEPS += \
./inc/MasterLib_Rev.d 


# Each subdirectory must supply rules for building sources it contributes
inc/%.o: ../inc/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__NEWLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__USE_CMSIS=CMSIS_CORE_LPC17xx -D__USE_CMSIS_DSPLIB=CMSIS_DSPLIB_CM3 -D__LPC17XX__ -I"C:\apes\docs\lcpxpresso-projects\ARM\LPC1769\oriented-cpp\MasterLib_Rev\inc" -I"C:\apes\docs\lcpxpresso-projects\ARM\LPC1769\oriented-cpp\CMSIS_CORE_LPC17xx\inc" -I"C:\apes\docs\lcpxpresso-projects\ARM\LPC1769\oriented-cpp\CMSIS_DSPLIB_CM3\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-rtti -fno-exceptions -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


