/*
 * SysTick_Lib.cpp
 *
 *  Created on: 03/04/2015
 *      Author: Thiago Mallon
 */

#include "SysTick_Lib.h"

/* configura SysTick */
Class_SysTick::Class_SysTick(void) {
	SysTick->CTRL |= (1 << 2); // fonte de decremento é C_CLK
}

/* implementa delay em microssegundos */
void Class_SysTick::delayUs(uint32_t us) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // atribui valor de reinício do contador, equivalente à 1us
	static uint32_t count; // contador de 'zeramentos'
	SysTick->CTRL |= (1 << 0); // inicializa contador do SysTick
	for (count = 0; count < us; count++) { // executa até que count se iguale à us
		do { // aguarda até que contador do SysTick zere
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador do SysTick
}

/* implementa delay em milissegundos */
void Class_SysTick::delayMs(uint32_t ms) {
	SysTick->LOAD = (SystemCoreClock / 1000) - 1; // atribui valor de reinício do contador, equivalente à 1ms
	static uint32_t count; // contador de 'zeramentos'
	SysTick->CTRL |= (1 << 0); // inicializa contador do SysTick
	for (count = 0; count < ms; count++) { // executa até que count se iguale à us
		do { // aguarda até que contador do SysTick zere
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador do SysTick
}
