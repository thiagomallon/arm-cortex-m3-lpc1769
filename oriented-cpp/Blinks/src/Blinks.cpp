/*
 ===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>
#include "SysTick_Lib.h"

/* protótipos */
void ledsConfig(void); // configura pinos de ledss

int main(void) {
	SystemInit(); // inicializa sistema - C_CLK = 100MHz, PCLK = 25MHz, etc
	SystemCoreClockUpdate(); // atualiza valor de clock na variável SystemCoreClock
	SCB->AIRCR |= 0x05FA0000 | 0b100 << 8; // prigroup b100 - 8 níveis e 4 subníveis de prioridade de interrupção
	Class_SysTick SysTick_Obj;
	ledsConfig(); // configura pinos de leds
	do {
		SysTick_Obj.delayMs(500); // implementa delay de 500ms
		LPC_GPIO1->FIOPIN ^= (1 << 28 | 1 << 29);
		LPC_GPIO3->FIOPIN ^= (1 << 25 | 1 << 26);
	} while (1);
	return 1;
}

/* configura pinos de leds */
void ledsConfig(void) {
	/* pinos P1.28 e P1.29 */
	LPC_PINCON->PINSEL3 &= ~(0b11 << 24 | 0b11 << 26); // pinos como GPIO
	LPC_GPIO1->FIODIR |= (1 << 28 | 1 << 29); // pinos como saída
	LPC_GPIO1->FIOPIN &= ~(1 << 28 | 1 << 29); // estado lógico baixo
	/* pinos P3.25 e P3.26 */
	LPC_PINCON->PINSEL7 &= ~(0b11 << 18 | 0b11 << 20); // pinos como GPIO
	LPC_GPIO3->FIODIR |= (1 << 25 | 1 << 26); // pinos como saída
	LPC_GPIO3->FIOPIN &= ~(1 << 25 | 1 << 26); // estado lógico baixo
}
