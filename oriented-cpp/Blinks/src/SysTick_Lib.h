/*
 * SysTick_Lib.h
 *
 *  Created on: 03/04/2015
 *      Author: Thiago Mallon
 */

#ifndef SYSTICK_LIB_H_
#define SYSTICK_LIB_H_

#include <LPC17xx.h>

class Class_SysTick {
private:
public:
	Class_SysTick(void);
	void delayUs(uint32_t);
	void delayMs(uint32_t);
};

#endif /* SYSTICK_LIB_H_ */
