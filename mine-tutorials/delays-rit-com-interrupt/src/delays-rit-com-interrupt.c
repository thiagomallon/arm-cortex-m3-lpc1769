/*
 ===============================================================================
 Name        : delays-rist-com-interrupt.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

/* variáveis globais */
static unsigned int ritCounter; // contador de interrupções

/* protótipos */
void ritConfig(void); // configura rit
void delayUs(unsigned int us); // implementa delay em us
void delayMs(unsigned int ms); // implementa delay em ms
void ledsConfigs(void); // configura pinos de leds

/* isr do rit */
void RIT_IRQHandler(void) {
	ritCounter++; // incrementa contador de interrupções
	LPC_RIT->RICTRL |= (1 << 0); // limpa flag de interrupção
}

int main(void) {
	SystemInit(); // inicializa sistema
	SystemCoreClockUpdate(); // atualiza valor de c_clk para variável 'SystemCoreClock'
	SCB->AIRCR |= 0x05FA0000 | 0b100 << 8;
	ritConfig();
	ledsConfigs();
	do { // loop infinito
		delayMs(500);
		LPC_GPIO1->FIOPIN ^= (1 << 29);
	} while (1);
	return 1;
}

/* configura rit */
void ritConfig(void) {
	LPC_SC->PCLKSEL1 = ((LPC_SC->PCLKSEL1 & ~(0b11 << 26)) | (0b01 << 26)); // pclk_rit = c_clk
	LPC_SC->PCONP |= (1 << 16); // habilita periférico rit
	LPC_RIT->RICOUNTER = 0; // zera contador do rit
	LPC_RIT->RICOMPVAL = (SystemCoreClock / 1000000); // atribui valor de comparação, para interrupção
	LPC_RIT->RICTRL |= (1 << 0 | 1 << 1 | 1 << 2); // limpa flag de interrupção do rit; reinicia contador após interrupção; depuração afeta rit
	NVIC->IP[29] |= (0 << 4 | 1 << 3);
	NVIC->ISER[0] |= (1 << 29);
	NVIC->ICPR[0] |= (1 << 29);
}

/* implementa delay em us */
void delayUs(unsigned int us) {
	ritCounter = 0; // zera contador de interrupções
	LPC_RIT->RICTRL |= (1 << 0 | 1 << 3); // limpa flag de interrupção do rit; habilita contador
	do { // aguarda até que valor do contador de interrupções se iguale ao valor do parâmetro recebido
	} while (ritCounter < us);
	LPC_RIT->RICTRL &= ~(1 << 3); // para contador
}

/* implementa delay em ms */
void delayMs(unsigned int ms) {
	ritCounter = 0; // zera contador de interrupções
	LPC_RIT->RICTRL |= (1 << 0 | 1 << 3); // limpa flag de interrupção do rit; habilita contador
	do { // aguarda até que valor do contador de interrupções se iguale ao valor do parâmetro recebido
	} while (ritCounter < (ms * 1000));
	LPC_RIT->RICTRL &= ~(1 << 3); // para contador
}

/* configura pinos de leds */
void ledsConfigs(void) {
	/* pinos p1.28 e p1.29 */
	LPC_PINCON->PINSEL3 &= ~(0b11 << 24 | 0b11 << 26); // pinos como gpio
	LPC_GPIO1->FIODIR |= (1 << 28 | 1 << 29); // pinos como saída
	LPC_GPIO1->FIOPIN |= (1 << 28 | 1 << 29); // estado lógico baixo
}
