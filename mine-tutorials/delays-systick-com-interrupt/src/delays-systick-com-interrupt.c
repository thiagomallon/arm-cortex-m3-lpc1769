/*
 ===============================================================================
 Name        :
 Author      : Thiago Mallon
 Version     :
 Copyright   :
 Description :

 Cálculo para registrador LOAD do SysTick, é feito da seguinte forma:
 - primeiro calcula-se a frequência, com relação ao tempo de intervalo desejado (Ex: 1ms): ((1 / 1 * 1000) = 1000Hz ou 1KHz
 - calcula-se valor, com base no clock atribuído à CPU (C_CLK): ((100MHz / 1000) -1) = 99999

 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

/* variáveis globais */
static unsigned int sysTickCounter; // contador de interrupções do systick

/* protótipos */
void sysTickConfig(void); // configura systick
void delayUs(unsigned int us);
void delayMs(unsigned int ms); // implementa delay em ms (milisegundos)
void ledsConfig(void); // configura pinos de leds

/* isr do systick */
void SysTick_Handler(void) {
	sysTickCounter++; // incrementa variável
}

int main(void) {
	SystemInit(); // inicializa sistema - c_clk = 100mhz, pclk = 25mhz, etc.
	SCB->AIRCR |= 0x05FA0000 | 0b100 << 8; // prigroup b100 - 8 níveis e 4 subníveis de prioridades de interrupção
	sysTickConfig();
	ledsConfig();
	do { // loop infinito
		delayMs(500);
		LPC_GPIO1->FIOPIN ^= (1 << 28 | 1 << 29);
	} while (1);
	return 1;
}

/* configura systick */
void sysTickConfig(void) {
	SystemCoreClockUpdate(); // atualiza valor de c_clk
	SCB->SHP[11] |= (0 << 4 | 1 << 3); // nível 0 e subnível 1 de prioridade de interrupção
	SysTick->CTRL |= (1 << 1 | 1 << 2); // habilita interrupções; fonte de decremento será c_clk
}

/* implementa delay em ms */
void delayMs(unsigned int ms) {
	sysTickCounter = 0; // zera contador de interrupções
	SysTick->LOAD = (SystemCoreClock / 1000) - 1; // valor de reinício do contador
	SysTick->CTRL |= (1 << 0); // inicializa contador
	do { // aguarda até que valor do contador se iguale a valor recebido via parâmetro
	} while (sysTickCounter < ms);
	SysTick->CTRL &= ~(1 << 0); // inicializa contador
}

/* implementa delay em us */
void delayUs(unsigned int us) {
	sysTickCounter = 0; // zera contador de interrupções
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // valor de reinício do contador
	SysTick->CTRL |= (1 << 0); // inicializa contador
	do { // aguarda até que valor do contador se iguale a valor recebido via parâmetro
	} while (sysTickCounter < us);
	SysTick->CTRL &= ~(1 << 0); // para contador
}

/* configura pinos dos leds */
void ledsConfig(void) {
	/* pinos p1.28 e p1.29 */
	LPC_PINCON->PINSEL3 &= ~(0b11 << 24 | 0b11 << 26); // pinos como gpio
	LPC_GPIO1->FIODIR |= (1 << 28 | 1 << 29); // pinos como saída
	LPC_GPIO1->FIOPIN &= ~(1 << 28 | 1 << 29); // estado lógico baixo
}
