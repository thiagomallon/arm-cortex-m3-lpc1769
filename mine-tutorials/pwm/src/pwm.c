/*
 ===============================================================================
 Name        : pwm.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

/* protótipos */
void pwmInit(void);
void pwmPinsConfig(void);

int main(void) {
	SystemInit();
	SCB->AIRCR |= 0x05FA0000 | 0b100 << 8;
	pwmInit();
	pwmPinsConfig();
	do {
	} while (1);
	return 1;
}

/* configura/inicializa pwm */
void pwmInit(void) {
	LPC_SC->PCONP |= (1 << 6); // habilita periférico pwm
	LPC_SC->PCLKSEL0 = ((LPC_SC->PCLKSEL0 & ~(0b11 << 12)) | (0b01 << 12)); // pclk_pwm = c_clk
	LPC_PWM1->CTCR &= ~(0b11 << 0); // fonte de incremento é borda de subida de pclk_pwm
	LPC_PWM1->TCR |= (1 << 3 | 1 << 1 | 1 << 0); // modo pwm; reinicia contadores PWM1TC e PWM1PC; inicializa contadores PWM1TC e PWM1PC
	LPC_PWM1->MR0 = 999999;
	LPC_PWM1->MR1 = 23499;
	NVIC->IP[9] |= (0 << 4 | 0 << 3);
	NVIC->ISER[0] |= (1 << 9);
	NVIC->ICPR[0] |= (1 << 9);
}

/* configura pinos do pwm */
void pwmPinsConfig(void) {
	/* pinos p1.18, p1.20, p1.21, p1.23, p1.24, p1.26 */
	LPC_PINCON->PINSEL3 = ((LPC_PINCON->PINSEL3
			& ~(0b11 << 4 | 0b11 << 8 | 0b11 << 10 | 0b11 << 14 | 0b11 << 16
					| 0b11 << 20))
			| (0b10 << 4 | 0b10 << 8 | 0b10 << 10 | 0b10 << 14 | 0b10 << 16
					| 0b10 << 20)); // pinos como saída do pwm
}
