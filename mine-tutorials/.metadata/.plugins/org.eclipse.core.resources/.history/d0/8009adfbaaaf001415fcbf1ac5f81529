/*
 ===============================================================================
 Name        : pullups_pulldowns.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

/* variáveis globais */
unsigned int sysTickCounter; // contador de interrupções

/* protótipos */
void sysTickConfig(void);
void delayUs(unsigned int us);
void ledsConfig(void);
void buttonsConfig(void);

/* isr do systick */
void SysTick_Handler(void) {
	sysTickCounter++; // incrementa contador de interrupções
}

int main(void) {
	SystemInit();
	SystemCoreClockUpdate();
	SCB->AIRCR |= 0x05FA0000 | 0b100 << 8;
	sysTickConfig();
	ledsConfig();
	buttonsConfig();
	do {
		if (!(LPC_GPIO0->FIOPIN & (1 << 4))) {
			LPC_GPIO1->FIOPIN &= ~(1 << 28 | 1 << 29); // estado lógico baixo
			LPC_GPIO3->FIOPIN &= ~(1 << 26 | 1 << 26);
			delayUs(250000);
		}
		if (!(LPC_GPIO0->FIOPIN & (1 << 5))) {
			LPC_GPIO1->FIOPIN &= ~(1 << 28 | 1 << 29); // estado lógico baixo
			LPC_GPIO3->FIOPIN &= ~(1 << 26 | 1 << 26);
			delayUs(250000);
		}
		if (!(LPC_GPIO0->FIOPIN & (1 << 10))) {
			LPC_GPIO1->FIOPIN &= ~(1 << 28 | 1 << 29); // estado lógico baixo
			LPC_GPIO3->FIOPIN &= ~(1 << 26 | 1 << 26);
			delayUs(250000);
		}
		if (!(LPC_GPIO0->FIOPIN & (1 << 11))) {
			LPC_GPIO3->FIOPIN &= ~(1 << 25);
			delayUs(250000);
		}
	} while (1);
	return 1;
}

/* configura pinos de leds */
void ledsConfig(void) {
	/* pinos p1.28 e p1.29 */
	LPC_PINCON->PINSEL3 &= ~(0b11 << 24 | 0b11 << 26); // pinos como gpio
	LPC_GPIO1->FIODIR |= (1 << 28 | 1 << 29); // pinos como saída
	LPC_GPIO1->FIOPIN &= ~(1 << 28 | 1 << 29); // estado lógico baixo
	/* pinos p3.25 e p3.26 */
	LPC_PINCON->PINSEL7 &= ~(0b11 << 18 | 0b11 << 20);
	LPC_GPIO3->FIODIR |= (1 << 25 | 1 << 26);
	LPC_GPIO3->FIOPIN &= ~(1 << 25 | 1 << 26);
}

/* configura pinos de botões */
void buttonsConfig(void) {
	/* pinos p0.4, p0.5, p0.10 e p0.11 */
	LPC_PINCON->PINSEL0 &= ~(0b11 << 8 | 0b11 << 10 | 0b11 << 20 | 0b11 << 22);
	LPC_PINCON->PINMODE0 &= ~(0b11 << 8 | 0b11 << 10 | 0b11 << 20 | 0b11 << 22);
	LPC_GPIO0->FIODIR &= ~(1 << 4 | 1 << 5 | 1 << 10 | 1 << 11);
	LPC_GPIO0->FIOPIN |= (1 << 4 | 1 << 5 | 1 << 10 | 1 << 11);
}

/* configura systick */
void sysTickConfig(void) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // valor de reinício para o contador
	SCB->SHP[11] |= (0 << 4 | 1 << 3); // nível 0 e subnível 1 de prioridade de interrupção
	SysTick->CTRL |= (1 << 1 | 1 << 2); // habilita interrupções; fonte de decremento é c_clk
}

/* implementa delay em microsegundos */
void delayUs(unsigned int us) {
	sysTickCounter = 0; // zera contador
	SysTick->CTRL |= (1 << 0); // habilita contador
	do { // aguarda até que valor do contador se iguale ao valor recebido via parâmetro
	} while (sysTickCounter < us);
	SysTick->CTRL &= ~(1 << 0); // para contador
}
