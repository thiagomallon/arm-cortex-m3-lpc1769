/*
 ===============================================================================
 Name        : delays-rit.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

void ritConfig(void); // configura rit
void delayUs(unsigned int us); // implementa delay em us
void delayMs(unsigned int ms); // implementa delay em ms
void ledsConfigs(void); // configura pinos de leds

int main(void) {
	SystemInit(); // inicializa sistema
	SCB->AIRCR |= 0x05FA0000 | 0b100 << 8;
	ritConfig();
	ledsConfigs();
	do { // loop infinito
		delayUs(500000);
		LPC_GPIO1->FIOPIN ^= (1 << 29);
	} while (1);
	return 1;
}

/* configura rit */
void ritConfig(void) {
	SystemCoreClockUpdate();
	LPC_SC->PCONP |= (1 << 16); // habilita periférico rit
	LPC_SC->PCLKSEL1 = ((LPC_SC->PCLKSEL1 & ~(0b11 << 26)) | (0b01 << 26)); // pclk_rit = c_clk
	LPC_RIT->RICTRL |= (1 << 0 | 1 << 1); // limpa flag de interrupção do rit
}

/* implementa delay em us */
void delayUs(unsigned int us) {
	LPC_RIT->RICOMPVAL = (((float) us / 1000000) * SystemCoreClock);
	LPC_RIT->RICTRL |= (1 << 0 | 1 << 3); // limpa flag de interrupção do rit; habilita contador
	do {
	} while (!(LPC_RIT->RICTRL & (1 << 0)));
	LPC_RIT->RICTRL &= ~(1 << 3); // para contador
}

/* implementa delay em ms */
void delayMs(unsigned int ms) {
	LPC_RIT->RICOMPVAL = (((float) ms / 1000) * SystemCoreClock);
	LPC_RIT->RICTRL |= (1 << 0 | 1 << 3); // limpa flag de interrupção do rit; habilita contador
	do {
	} while (!(LPC_RIT->RICTRL & (1 << 0)));
	LPC_RIT->RICTRL &= ~(1 << 3); // para contador
}

/* configura pinos de leds */
void ledsConfigs(void) {
	/* pinos p1.28 e p1.29 */
	LPC_PINCON->PINSEL3 &= ~(0b11 << 24 | 0b11 << 26); // pinos como gpio
	LPC_GPIO1->FIODIR |= (1 << 28 | 1 << 29); // pinos como saída
	LPC_GPIO1->FIOPIN |= (1 << 28 | 1 << 29); // estado lógico baixo
}
