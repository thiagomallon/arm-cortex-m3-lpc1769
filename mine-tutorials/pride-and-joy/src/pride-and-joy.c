/*
 ===============================================================================
 Name        : pride-and-joy.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

int main(void) {
	SystemInit();
	do {
	} while (1);
	return 1;
}

int operadores(void) {
	LPC_PINCON->PINSEL0 |= (0b11 << 12); // gpio
	LPC_GPIO0->FIODIR |= (1 << 22); // pino como saída
	LPC_GPIO0->FIOPIN &= ~(1 << 22); // estado lógico baixo
	LPC_GPIO0->FIOPIN ^= (1 << 22); // estado lógico baixo
}
