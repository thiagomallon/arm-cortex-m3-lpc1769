/*
 ===============================================================================
 Name        : systick-n-interrupt.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

/* protótipos */
void sysTickConfig(void);
void delayUs(unsigned int us);
void ledsConfig(void);

int main(void) {
	SystemInit();
	sysTickConfig();
	ledsConfig();
	do { // loop infinito
		delayUs(500000);
		LPC_GPIO1->FIOPIN ^= (1 << 28 | 1 << 29);
	} while (1);
	return 1;
}

/* configura systick */
void sysTickConfig(void) {
	SystemCoreClockUpdate(); // atualiza valor de clock para variável SystemCoreClock
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1;
	SysTick->CTRL |= (1 << 2); // habilita interrupção; fonte de decremento é c_clk
}

/* implementa delay em us */
void delayUs(unsigned int us) {
	static unsigned int counter; // variável contadora de interrupções
	SysTick->CTRL |= 1 << 0; // inicializa contador do systick
	for (counter = 0; counter < us; counter++) {
		do { // aguarda até que seja gerada interrupção no systick
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador do systick
}

/* configura pinos de led */
void ledsConfig(void) {
	/* pinos p1.28 e p1.29 */
	LPC_PINCON->PINSEL3 &= ~(0b11 << 24 | 0b11 << 26);
	LPC_GPIO1->FIODIR |= (1 << 28 | 1 << 29);
	LPC_GPIO1->FIOPIN &= ~(1 << 28 | 1 << 29);
}
